import os, fnmatch, zipfile, shutil
from pathlib import Path

# set sting for location of part specs folder
basePath = "C:\\Users\\orderentry\\Desktop\\Part specs\\"
endPath = "C:\\Users\\orderentry\\Desktop\\Acumatica\\"

# searches for the next folder w/out 'done'
def SearchforNext():
    # iterate through Part specs folder
    for part in Path(basePath).iterdir():
        # check if the folder ends with 'Done'
        if (fnmatch.fnmatch(part,'*Done')):
            pass
        else:
            # create a folder in the acumatica folder with the same name as in the part specs
            acuPath = endPath + str(part.name)
            try: 
                os.mkdir(acuPath)
            except FileExistsError: 
                pass 
            # check for files larger than 50,000KB
            MoveLargeFiles(part)
            # zip any folders in the part folder
            ZipAllFolders(part)
            # copy files to the acumatica folder 
            CopyFiles(part, acuPath)
            # add -Done to the end of the folder name
            os.rename(part, str(part) + '-Done')

# move any files larger than 50,000KB to a new folder
def MoveLargeFiles(filePath):
    # make variable with path to the large files folder
    largeFiles = Path(str(filePath) + "\Large Files")
    # iterate through the part folder
    for file in filePath.iterdir():
        # get size in KB
        fileSize = (Path(file).stat().st_size)/1024
        # check if file, if larger than 50,0000KB, and if not a zip file
        if (file.is_file() == True and fileSize > 50000 and not fnmatch.fnmatch(file, '*.zip')):
            # create folder if it doesn't exist
            try:
                largeFiles.mkdir()
            except FileExistsError: 
                pass
            # copy large file to folder
            shutil.copy2(file, largeFiles)
            # delete file once moved
            os.remove(file)


# zip any folders in the part spec folder
def ZipAllFolders(folderPath):
    # iterate through the part folder 
    for folder in folderPath.iterdir():
        # check if directory 
        if(folder.is_dir()): 
            # set to where zipped folder will be located
            zipFolder = str(folder) + '.zip'
            # create zipped folder
            with zipfile.ZipFile(zipFolder, mode='w') as archive: 
                # iterate through the folder that is being zipped 
                for file in folder.iterdir(): 
                    # add to zipped folder, but keep current name 
                    archive.write(file, arcname=file.name)


# copy all files from the part spec folder to the acumatica folder
def CopyFiles(oldPath, newPath): 
    # iterate through the part folder 
    for file in oldPath.iterdir(): 
        # check if file 
        if(file.is_file()): 
            # move to acumatica folder
            acuFolder = newPath + '\\' + file.name
            shutil.copy(file, acuFolder)
        
SearchforNext()